package com.tcwgq.utils;

import java.io.File;
import java.io.InputStream;


public class OssTemplate {

    /**
     * 上传文件
     *
     * @param key  文件在oss的唯一标识
     * @param file 待上传文件
     * @return cdn + key的文件url地址
     */
    public static String uploadFile(String key, File file) {
        return "";
    }

    /**
     * 上传文件
     *
     * @param key     文件在oss的唯一标识
     * @param content 待上传内容
     * @return cdn + key的文件url地址
     */
    public static String uploadFile(String key, String content) {
        return "";
    }


    /**
     * 上传文件
     *
     * @param key 文件在oss的唯一标识
     * @param in  待上传内容
     * @return cdn + key的文件url地址
     */
    public static String uploadFile(String key, InputStream in) {
        return "";
    }

}

package com.tcwgq.controller.upload;

import com.baidu.ueditor.ActionEnter;
import com.tcwgq.model.UeditorImageDTO;
import com.tcwgq.service.UploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author tcwgq
 * @date 2020/3/17 17:19
 */
@RestController
@RequestMapping("/image/")
public class UploadController {
    @Autowired
    private UploadService uploadService;

    @PostMapping("/upload")
    UeditorImageDTO upload(@RequestParam("file") MultipartFile file) throws IOException {
        String originalFilename = file.getOriginalFilename();
        return uploadService.upload(file.getInputStream(), originalFilename);
    }

    @GetMapping("/init")
    void init(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String exec = new ActionEnter(request, "/").exec();
        System.out.println(exec);

        response.getWriter().write(new ActionEnter(request, "/").exec());
    }
}

package com.tcwgq.controller.upload;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author tcwgq
 * @date 2020/3/17 16:41
 */
@Controller
@RequestMapping("/index")
public class IndexController {
    @GetMapping("")
    public String hello() {
        return "index";
    }
}

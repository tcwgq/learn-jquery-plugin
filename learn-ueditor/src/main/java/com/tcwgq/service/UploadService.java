package com.tcwgq.service;

import com.tcwgq.model.UeditorImageDTO;

import java.io.File;
import java.io.InputStream;

/**
 * @author tcwgq
 * @date 2020/3/16 18:54
 */
public interface UploadService {
    String upload(String content);

    String upload(File file);

    String upload(InputStream is);

    UeditorImageDTO upload(InputStream is, String filename);
}

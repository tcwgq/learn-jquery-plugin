package com.tcwgq.service.impl;

import com.alibaba.fastjson.JSON;
import com.tcwgq.model.UeditorImageDTO;
import com.tcwgq.service.UploadService;
import com.tcwgq.utils.OssTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.InputStream;
import java.util.UUID;

/**
 * @author tcwgq
 * @date 2020/3/16 18:41
 */
@Slf4j
@Service
public class UploadServiceImpl implements UploadService {
    private static final String PREFIX = "iyb-app/img/";

    public String upload(String content) {
        return OssTemplate.uploadFile(uuid(), content);
    }

    public String upload(File file) {
        return OssTemplate.uploadFile(uuid(), file);
    }

    @Override
    public String upload(InputStream is) {
        return OssTemplate.uploadFile(uuid(), is);
    }

    @Override
    public UeditorImageDTO upload(InputStream is, String filename) {
        String url = OssTemplate.uploadFile(uuid(), is);
        UeditorImageDTO dto = new UeditorImageDTO();
        dto.setState("SUCCESS");
        dto.setTitle(filename);
        dto.setOriginal(filename);
        dto.setUrl(url);
        log.info("上传结果为 {}", JSON.toJSONString(dto));

        return dto;
    }

    private String uuid() {
        return PREFIX + UUID.randomUUID().toString().replace("-", "");
    }
}

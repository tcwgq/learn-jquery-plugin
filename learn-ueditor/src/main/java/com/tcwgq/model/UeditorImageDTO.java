package com.tcwgq.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @author tcwgq
 * @date 2020/3/17 18:57
 */
@Data
public class UeditorImageDTO implements Serializable {
    private String state;
    private String url;
    private String title;
    private String original;
}

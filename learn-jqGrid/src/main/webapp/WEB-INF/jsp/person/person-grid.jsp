<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>jqGrid</title>
		<meta name="description" content="Dynamic tables and grids using jqGrid plugin" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<link rel="stylesheet" href="/assets/css/bootstrap.min.css" />
		<link rel="stylesheet" href="/assets/font-awesome/4.5.0/css/font-awesome.min.css" />
		<link rel="stylesheet" href="/assets/css/ui.jqgrid.min.css" />
		<link rel="stylesheet" href="/assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

	</head>

	<body class="no-skin">
			<div class="main-content">
				<div class="main-content-inner">
					<div class="page-content">
						<div class="row">
						<button onclick="doSearch();">search</button>
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<table id="grid-table"></table>
								<div id="grid-pager"></div>
								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->
		</div><!-- /.main-container -->

		<script src="/assets/js/jquery-2.1.4.min.js"></script>
		<script src="/assets/js/jquery.jqGrid.min.js"></script>
		<script src="/assets/js/grid.locale-en.js"></script>

		<script type="text/javascript">
			function doSearch(){
				jQuery("#grid-table").jqGrid('setGridParam',{ 
	                url:"/person/getPersons", 
	                postData:{id:1},
	                page:1
	            }).trigger("reloadGrid"); 
			}
			
			jQuery(function($) {
				var grid_selector = "#grid-table";
				var pager_selector = "#grid-pager";
				
				var parent_column = $(grid_selector).closest('[class*="col-"]');
				$(window).on('resize.jqGrid', function () {
					$(grid_selector).jqGrid( 'setGridWidth', parent_column.width() );
			    })
				
				$(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
					if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
						setTimeout(function() {
							$(grid_selector).jqGrid( 'setGridWidth', parent_column.width() );
						}, 20);
					}
			    })
				
				jQuery(grid_selector).jqGrid({
					url: '/person/getPersons',
					mtype: 'GET',
					datatype: "json",
					height: 250,
					//loadonce: true,
					colNames:['id','name','age', 'timeCreated', 'timeModified'],
					colModel:[
						{name:'id',index:'id', width:60},
						{name:'name',index:'sdate',width:90},
						{name:'age',index:'age', width:150},
						{name:'timeCreated',index:'timeCreated', width:70},
						{name:'timeModified',index:'timeModified', width:90}
					], 
					viewrecords : true,
					rowNum:10,
					rowList:[10,20,30],
					pager : pager_selector,
					altRows: true,
					//toppager: true,
					multiselect: true,
					//multikey: "ctrlKey",
			        multiboxonly: true,
			        jsonReader : {  
	        	      root:"dataList",  
	        	      page: "page",  
	        	      total: "total",  
	        	      records: "records",  
	        	      repeatitems: false,  
	        	      id: "id"  
			        },
					loadComplete : function() {
						var table = this;
						setTimeout(function(){
							updatePagerIcons(table);
						}, 0);
					},
					caption: "Person List"
				});
				
				$(window).triggerHandler('resize.jqGrid');
			
				function updatePagerIcons(table) {
					var replacement = 
					{
						'ui-icon-seek-first' : 'ace-icon fa fa-angle-double-left bigger-140',
						'ui-icon-seek-prev' : 'ace-icon fa fa-angle-left bigger-140',
						'ui-icon-seek-next' : 'ace-icon fa fa-angle-right bigger-140',
						'ui-icon-seek-end' : 'ace-icon fa fa-angle-double-right bigger-140'
					};
					$('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon').each(function(){
						var icon = $(this);
						var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
						
						if($class in replacement) icon.attr('class', 'ui-icon '+replacement[$class]);
					})
				}
			
				function enableTooltips(table) {
					$('.navtable .ui-pg-button').tooltip({container:'body'});
					$(table).find('.ui-pg-div').tooltip({container:'body'});
				}
			
				$(document).one('ajaxloadstart.page', function(e) {
					$.jgrid.gridDestroy(grid_selector);
					$('.ui-jqdialog').remove();
				});
			});
		</script>
	</body>
</html>

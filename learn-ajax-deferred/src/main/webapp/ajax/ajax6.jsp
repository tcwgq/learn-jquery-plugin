<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- ${pageContext.request.contextPath } 这种方式直接取不出来值，怎么回事？-->
<script type="text/javascript" src="/js/jquery-2.1.4.min.js"></script>
</head>
<body>
	<h1>Deferred对象的使用</h1>
</body>
<script type="text/javascript">
	//另一种防止执行状态被外部改变的方法，是使用deferred对象的建构函数$.Deferred()。
	var wait = function(dtd) {
		var tasks = function() {
			alert('执行完毕');
			//dtd.resolve();//改变deferred对象的执行状态，由未完成变为已完成，从而调用done中的回调方法
			dtd.reject();//改变deferred对象的执行状态，由未完成变为已失败，从而调用fail中的回调方法
		};
		setTimeout(tasks, 5000);
	};
	//jQuery规定，$.Deferred()可以接受一个函数名（注意，是函数名）作为参数，$.Deferred()所生成的deferred对象将作为这个函数的默认参数。
	$.Deferred(wait).done(function() {
		alert("哈哈，成功了！");
	}).fail(function() {
		alert("出错啦！");
	});
</script>
</html>
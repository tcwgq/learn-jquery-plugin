<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- ${pageContext.request.contextPath } 这种方式直接取不出来值，怎么回事？-->
<script type="text/javascript" src="/js/jquery-2.1.4.min.js"></script>
</head>
<body>
	<h1>Deferred对象的使用</h1>
</body>
<script type="text/javascript">
	var dtd = $.Deferred();
	var wait = function(dtd) {
		var tasks = function() {
			alert('执行完毕');
			dtd.resolve();//改变deferred对象的执行状态，由未完成变为已完成，从而调用done中的回调方法
		};
		setTimeout(tasks, 5000);
	};
	//这里的关键是dtd.promise(wait)这一行，它的作用就是在wait对象上部署Deferred接口。正是因为有了这一行，后面才能直接在wait上面调用done()和fail()。
	dtd.promise(wait);
	wait.done(function() {
		alert("哈哈，成功了！");
	}).fail(function() {
		alert("出错啦！");
	});
	wait(dtd);
	
	deferred.then();//把done和fail方法写在一起
	$.when($.ajax( "/main.php" )).then(successFunc, failureFunc );
	//如果then()有两个参数，那么第一个参数是done()方法的回调函数，第二个参数是fail()方法的回调方法。如果then()只有一个参数，那么等同于done()。
	deferred.always();//不管调用的是deferred.resolve()还是deferred.reject()，最后总是执行。
	$.ajax( "test.html" ).always( function() { alert("已执行！");} );
</script>
</html>
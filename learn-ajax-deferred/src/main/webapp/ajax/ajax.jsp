<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- ${pageContext.request.contextPath } 这种方式直接取不出来值，怎么回事？-->
<script type="text/javascript" src="/js/jquery-2.1.4.min.js"></script>
</head>
<body>
	<h1>Deferred对象的使用</h1>
</body>
<script type="text/javascript">
	//传统写法，1.5.0以前，返回的是XHR对象
	$.ajax({
		url : 'test.jsp',
		success : function() {
			alert('成功了！');
		},
		error : function() {
			alert('失败了！');
		}
	});
	//1.5.0以后，返回的是Deferred对象，可以使用链式编程
	$.ajax('test1.jsp').success(function() {
		alert('成功了！');
	}).fail(function() {
		alert('失败了！');
	}).done(function() {
		alert('第二个回调函数');
	});
	//先执行两个异步请求，都成功了调用done中的回调函数，只要有一个不成功或者都失败，执行fail中的回调函数
	$.when($.ajax('test1.jsp'), $.ajax('test2.jsp')).done(function() {
		alert('哈哈，成功了！');
	}).fail(function() {
		alert('失败了');
	});
</script>
</html>